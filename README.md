# Test technique: Faire une Horloge avec alarmes.

## Quick Start

```
cd horloge
npm i
npm run start
```

## Description

L'objectif est ici de faire une horloge avec gestion d'alarmes.

## Technologies

Nous utiliserons ici [Electron](https://www.electronjs.org/fr/) pour faire une application de bureau, [React](https://fr.legacy.reactjs.org/) comme framework front-end afin de faciliter et améliorier nos développements. Le langage utilisé sera [TypeScript](https://www.typescriptlang.org/) et [webpack](https://webpack.js.org/) comme `module bundler`.

Les alarmes seront enregistrées en persistant à l'aide d'une BDD [SQLite](https://www.sqlite.org/index.html) déployée via Docker.

## Architecture

Voici un schéma de l'application : 

![schema](./schema.png)

## Améliorations possibles

On peut encore imaginer pas mal d'améliorations, en qualité de code et en features possibles.
Voici une liste de toutes celles imaginées, ce sont les issues sur le projet. J'ai détaillé chacune.
[A faire](https://framagit.org/Hyperion/horloge-electron-react/-/issues/?label_name%5B%5D=A%20faire)
const path = require('path');
const rootDir = process.cwd();

import type { ForgeConfig } from '@electron-forge/shared-types';
import { MakerSquirrel } from '@electron-forge/maker-squirrel';
import { MakerZIP } from '@electron-forge/maker-zip';
import { MakerDeb } from '@electron-forge/maker-deb';
import { MakerRpm } from '@electron-forge/maker-rpm';
import { AutoUnpackNativesPlugin } from '@electron-forge/plugin-auto-unpack-natives';
import { WebpackPlugin } from '@electron-forge/plugin-webpack';

import { mainConfig } from './config/webpack/webpack.main.config';
import { rendererConfig } from './config/webpack/webpack.renderer.config';

const config: ForgeConfig = {
  packagerConfig: {
    asar: true,
    executableName: 'Test technique: Horloge',

  },
  rebuildConfig: {}, // Non utilisé pour le momennt
  makers: [new MakerSquirrel({}), new MakerZIP({}, ['darwin']), new MakerRpm({}), new MakerDeb({})],
  plugins: [
    new AutoUnpackNativesPlugin({}),
    new WebpackPlugin({
      mainConfig,
      renderer: {
        config: rendererConfig,
        entryPoints: [
          {
            name: 'main_window',
            html: path.join(rootDir, 'src/renderer/index.html'),
            js: path.join(rootDir, 'src/renderer/renderer.tsx'),
            preload: {
              js: path.join(rootDir, 'src/renderer/preload.ts'),
            },
          },
        ],
      },
    }),
  ],
};

export default config;

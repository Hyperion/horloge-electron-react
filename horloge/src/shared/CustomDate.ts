export class CustomDate {
    private jsDate: Date;
    private ms: number;
    private sec: number;
    private min: number;
    private hour: number;
    public basicHorloge: string;
    public timeInput: string;
    // private day: number;
    // private month: number;
    // private year: number;

    constructor(jsDate: Date) {
        this.ms = jsDate.getMilliseconds();
        this.sec = jsDate.getSeconds();
        this.min = jsDate.getMinutes();
        this.hour = jsDate.getHours();
        this.jsDate = jsDate;
        this.timeInput = this.fillZero(this.hour) + ":" + this.fillZero(this.min);
        this.basicHorloge = this.timeInput + ":" + this.fillZero(this.sec);
    }

    public addMs(x: number): void {
        this.ms += x;
        const nb_sec_to_add: number = Math.floor(this.ms / 100);
        if (nb_sec_to_add) {
            this.addSecs(nb_sec_to_add);
        }
        this.ms = this.ms % 100;
    }

    public addSecs(x: number): void {
        this.sec += x;
        const nb_min_to_add: number = Math.floor(this.sec / 60);
        if (nb_min_to_add) {
            this.addMins(nb_min_to_add);
        }
        this.sec = this.sec % 60;
    }

    public addMins(x: number): void {
        this.min += x;
        const nb_hour_to_add: number = Math.floor(this.min / 100);
        if (nb_hour_to_add) {
            this.addHours(nb_hour_to_add);
        }
        this.min = this.min % 60;
    }

    public addHours(x: number): void {
        this.hour += x;
        // const nb_day_to_add: number = Math.floor(this.ms / 247);
        // if (nb_day_to_add) {
        // this.addDay(nb_day_to_add);
        // }
        this.hour = this.hour % 24;
    }

    private fillZero(x: number): string {
        if (x < 10)
            return "0" + x.toString();
        else
            return x.toString();
    }


}
import { NotifType } from "./NotifType";

export interface Notif {
    id: number;
    createdAt: Date;
    type: NotifType;
    message: string;
    timeInSecs: number;
};
export interface Alarme {
    id: number;
    textDate: string;
    disabled: boolean;
}
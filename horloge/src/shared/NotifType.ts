export enum NotifType{
    NONE,
    INFO,
    SUCCESS,
    ERROR,
};

import React from 'react';
import { createRoot } from 'react-dom/client';
import Appli from './app/app';

console.log('👋 This message is being logged by "renderer.js", included via webpack');


const app: React.JSX.Element = (
  <Appli />
);

// Render application in DOM
createRoot(document.getElementById('app')).render(app);



import React, { useEffect, useState } from 'react';

import BasicHorloge from './horloges/basic_horloge';
import { CustomDate } from '../../shared/CustomDate';
import PromptAlarme from './promptAlarme/prompt_alarme';
import { IpcService } from '../../main/ipc/IpcService';
import ListeAlarmes from './listeAlarmes/liste_alarmes';
import NowAlarme from './nowAlarme/now_alarme';
import NotifComponent from './notification/notification';
import { assets } from "../assets";

import './app.css';

const ipc = IpcService.getInstance();

const Appli: React.FC = () => {
    const [customDate, setCustomDate] = useState({});
    const [alarmes, setAlarmes] = useState([]);
    const [notifs, setNotifs] = useState([]);
    const [showPromptAlarme, setShowPromptAlarme] = useState(false);
    const [alarmeNow, setAlarmeNow] = useState(false);
    const [currentAlarme, setCurrentAlarme] = useState({ id: -1 });
    const [alarmeDisabled, setAlarmeDisabled] = useState(-1);

    function showAlarme() {
        setShowPromptAlarme(true);
    }

    const changeShowPromptAlarme = (value: boolean) => {
        setShowPromptAlarme(value);
    }

    const stopAlarme = (alarme: number) => {
        setAlarmeNow(false);
        setAlarmeDisabled(alarme);
    }

    const showAlarmeNow = (): boolean => {
        return alarmeNow && !(alarmeDisabled == currentAlarme.id);
    }

    ipc.receive("get-date", (value: { customDate: CustomDate }) => {
        setCustomDate(value.customDate);
    });

    ipc.receive("alarmes", (value: any) => {
        setAlarmes(value);
    });

    ipc.receive("now-alarmes", (value: any) => {
        setAlarmeNow(true)
        setCurrentAlarme(value[0]);
    });

    ipc.receive("notifs", (value: any) => {
        setNotifs(value);
    });

    return (
        <div className="main">
            <div className="header">
                <h1>Horloge</h1>
                <div className="settings">
                    <img height="48px" src={assets.settings} alt="parametres" />
                </div>
            </div>
            <div className="main-content">
                <div className="horloges">
                    {/* <div className="tabs-horloges"></div> */}
                    <div className="selected-horloge">
                        <BasicHorloge customDate={customDate} />
                    </div>
                </div>
                <hr />
                <div className="bottom-content">
                    <div className="minuteur column">
                        {/* <h2>Minuteur</h2> */}
                        {/* <hr /> */}
                    </div>
                    <div className="alarmes column">
                        <h2>Alarmes</h2>
                        <hr />
                        <ListeAlarmes alarmes={alarmes} />
                        <button className="add-alarme"
                            onClick={(e) => showAlarme()}
                        >Ajouter une alarme</button>
                    </div>
                    <div className="chronometre column">
                        {/* <h2>Chronomètre</h2> */}
                        {/* <hr /> */}

                    </div>
                </div>
            </div>
            <div className="popup-content">
                <div hidden={(!showPromptAlarme && !showAlarmeNow())} className="gray-screen"></div>
                <div className="prompt">
                    <PromptAlarme show={showPromptAlarme} changeShow={changeShowPromptAlarme} />
                </div>
                <div className="prompt">
                    <NowAlarme show={showAlarmeNow()} stopAlarme={stopAlarme} alarme={currentAlarme} />
                </div>
            </div>
            <div className="notifs">
                {notifs.map((item: any) => {
                    return <React.Fragment key={item.id}>
                        <NotifComponent notif={item} />
                    </React.Fragment>
                })}
            </div>
        </div>
    );
}


export default Appli;
import React, { ChangeEvent, useEffect, useState } from 'react';
import { IpcService } from '../../../main/ipc/IpcService';
import { IpcRequest } from '../../../shared/IpcRequest';
import './prompt_alarme.css';

const ipc = IpcService.getInstance();

const PromptAlarme: React.FC = ({ show, changeShow }: any) => {
    const [inputTime, setInputTime] = useState("");
    const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
        setInputTime(e.target.value);
    };
    function addAlarme() {
        const request: IpcRequest = {
            responseChannel: "",
            params: [inputTime]
        };
        ipc.send('add-alarme', request).then(value => {
            // console.log(value);
        });
        changeShow(false);
    };

    function undo() {
        changeShow(false);
    }

    return show && (
        <div className="pop-up">
            <div className="prompt-alarme">

                <h2>Ajouter une alarme</h2>
                <hr />
                <input className="timeInput" type="time" onChange={handleChange} value={inputTime} />
                <div className="btns">
                    <button className="btn1" onClick={(e) => undo()}>Annuler</button>
                    <button className="btn2" type="submit"
                        onClick={(e) => addAlarme()}>Valider</button>
                </div>
            </div>
        </div>

    );
}

export default PromptAlarme;
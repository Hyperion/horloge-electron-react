import React, { ChangeEvent, useEffect, useState } from 'react';
import './now_alarme.css';
import { IpcService } from '../../../main/ipc/IpcService';
import { IpcRequest } from '../../../shared/IpcRequest';
import { NotifType } from '../../../shared/NotifType';

const ipc = IpcService.getInstance();

const NowAlarme: React.FC = ({ show, stopAlarme, alarme }: any) => {
    function stop() {
        stopAlarme(alarme.id);
        const request: IpcRequest = {
            responseChannel: "",
            params: [NotifType.INFO.toString(), "Alarme stopée."]
        };
        ipc.send('add-notif', request).then(value => {
            // console.log(value);
        });
    }

    function repeatAlarme() {
        console.log("repeat");
    }

    return show && (
        <div className="pop-up">
            <div className="now-alarme">
                <h2>Alarme en cours</h2>
                <hr />
                <div className="current-alarme">
                    <p>{alarme.textDate}</p>
                </div>
                <hr />
                <div className="btns">
                    {/* <button className="btn1" onClick={(e) => repeatAlarme()}>Répéter</button> */}
                    <button className="btn2" type="submit"
                        onClick={(e) => stop()}>Arrêter</button>
                </div>
            </div>
        </div>

    );
}

export default NowAlarme;
import React, { useEffect, useState } from 'react';
import './notification.css';
import { Notif } from '../../../shared/Notif';
import { NotifType } from '../../../shared/NotifType';


const NotifComponent: React.FC = ({ notif }: { notif: Notif }) => {
    const getClass = (type: NotifType) => {
        switch (type) {
            case NotifType.NONE:
                return "notif";
            case NotifType.INFO:
                return "notif info";
            case NotifType.SUCCESS:
                return "notif success";
            case NotifType.ERROR:
                return "notif error";
            default:
                return "notif";
        }
    }
    const getName = (type: NotifType) => {
        switch (type) {
            case NotifType.NONE:
                return "notif";
            case NotifType.INFO:
                return "Info";
            case NotifType.SUCCESS:
                return "Succès !";
            case NotifType.ERROR:
                return "Erreur";
            default:
                return "notif";
        }
    }
    return (
        <div className={getClass(notif.type)}>
            <h3>{getName(notif.type)}</h3>
            <p>{notif.message}</p>
        </div>
    );
}

export default NotifComponent;
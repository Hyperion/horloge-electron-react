import React, { useEffect, useState } from 'react';
import './basic_horloge.css';


const BasicHorloge: React.FC = ({ customDate }: any) => {
    return (
        <div className="horloge">
            {customDate.basicHorloge}
        </div>
    );
}

export default BasicHorloge;
import React, { useEffect, useState } from 'react';
import './liste_alarmes.css';
import { assets } from '../../assets';
import { IpcService } from '../../../main/ipc/IpcService';
import { IpcRequest } from '../../../shared/IpcRequest';

const ListeAlarmes: React.FC = ({ alarmes }: any) => {
    const ipc = IpcService.getInstance();

    function removeAlarme(id: number) {
        const request: IpcRequest = {
            responseChannel: "",
            params: [id.toString()]
        };
        ipc.send('remove-alarme', request).then(value => {
            // console.log(value);
        });
    };

    function toogleAlarme(id: number) {
        const request: IpcRequest = {
            responseChannel: "",
            params: [id.toString()]
        };
        ipc.send('toogle-alarme', request).then(value => {
            // console.log(value);
        });
    };

    return (
        <ul className="alarmes-list">
            {alarmes.map((item: any) => {
                return (
                    <li key={item.id} className={item.disabled ? "alarme disabled" : "alarme"} >
                        <span className="dateAlarme">{item.textDate}</span>
                        <img className="removeAlarme" height="32px" src={assets.remove} alt="remove"
                            onClick={(e) => removeAlarme(item.id)} />
                        <img className="toogleAlarme" height="32px" src={item.disabled ? assets.alarm_off_grey : assets.alarm_on} alt="toogle"
                            onClick={(e) => toogleAlarme(item.id)} />

                    </li>
                )
            })}
        </ul>
    );
}

export default ListeAlarmes;
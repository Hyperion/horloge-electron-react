
import settings from "./../../assets/settings_white.png";
import remove from "./../../assets/delete.png";
import alarm_off_grey from "./../../assets/alarm_off_grey.png";
import alarm_on from "./../../assets/alarm_on.png";
import create from "./../../assets/create.png";

export const assets = {
    settings,
    remove,
    alarm_off_grey,
    alarm_on,
    create,
};

import { BrowserWindow } from "electron";
import { Notif } from "../shared/Notif";
import { NotifType } from "../shared/NotifType";

export class NotificationsManager {

    private static instance: NotificationsManager;
    private notifsCount = 0;
    private notifications: Map<number, Notif> = new Map<number, Notif>();
    private lastTime: number = 0;


    public static getInstance(): NotificationsManager {
        if (!NotificationsManager.instance) {
            NotificationsManager.instance = new NotificationsManager();
        }

        return NotificationsManager.instance;
    }

    constructor() {
    }

    public add(type: NotifType, message: string, time?: number) {

        this.notifsCount++;

        const notif: Notif = {
            id: this.notifsCount,
            createdAt: new Date(),
            type: type,
            message: message,
            timeInSecs: time ? time : 6
        }

        this.notifications.set(this.notifsCount, notif);
        this.delete(notif.id);
    }

    private delete(id: number) {
        setTimeout(() => {
            this.notifications.delete(id);
        }, this.notifications.get(id).timeInSecs * 1000);
    }

    public synchroNotifs(window: BrowserWindow) {
        if (this.notifications.size > 0 || this.lastTime > 0) {
            window.webContents.send('notifs', this.notificationsArray);
            this.lastTime = this.notifications.size
        }
    }

    get notificationsArray(): Notif[] {
        return [...this.notifications]
            .map(x => x[1])
            .sort((a, b) => a.id - b.id);
    }

}

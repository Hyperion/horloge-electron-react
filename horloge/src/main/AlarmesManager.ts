import { BrowserWindow } from "electron";
import { Alarme } from "../shared/Alarme";
import { DbManager } from "./DbManager";
import { CustomDate } from "../shared/CustomDate";
import { NotificationsManager } from "./NotificationsManager";
import { NotifType } from "../shared/NotifType";

export class AlarmesManager {

    private static instance: AlarmesManager;
    private upToDate: boolean;
    private alarmes: Alarme[];
    private dbManager: DbManager;
    private notificationsManager: NotificationsManager;

    public static getInstance(): AlarmesManager {
        if (!AlarmesManager.instance) {
            AlarmesManager.instance = new AlarmesManager();
        }

        return AlarmesManager.instance;
    }

    constructor() {
        this.dbManager = DbManager.getInstance();
        this.notificationsManager = NotificationsManager.getInstance();
        this.upToDate = false;
    }

    public addAlarme(newAlarme: any) {
        if (!this.alarmes.map(x => x.textDate).includes(newAlarme)) {

            this.dbManager.add("alarmes", { textDate: newAlarme, disabled: 0 }).then(value => {
                this.notificationsManager.add(NotifType.SUCCESS, "L'alarme a bien été ajoutée.");
                this.upToDate = false;
            }, err => {
                console.error(err);
                this.notificationsManager.add(NotifType.ERROR, err);
            });
        }
        else {
            this.notificationsManager.add(NotifType.ERROR, "Alarme déjà existante");
        }
    }

    public removeAlarme(id: number) {
        if (this.alarmes.map(x => x.id).includes(id)) {

            this.dbManager.remove("alarmes", id).then(value => {
                this.notificationsManager.add(NotifType.SUCCESS, "L'alarme a bien été supprimée.");
                this.upToDate = false;
            }, err => {
                console.error(err);
                this.notificationsManager.add(NotifType.ERROR, err);
            });
        }
        else {
            this.notificationsManager.add(NotifType.ERROR, "Alarme déjà supprimée.");
        }
    }

    public toogleAlarme(id: number) {
        const filterAlarmes = this.alarmes.filter(x => x.id === id);

        if (filterAlarmes.length > 0) {

            const alarme: Alarme = filterAlarmes[0];
            const disabledValue: string = !alarme.disabled ? "1" : "0";

            this.dbManager.update("alarmes", alarme.id, [{ column: "disabled", value: disabledValue}]).then(value => {
                const mess = "L'alarme a bien été "+ (alarme.disabled ? "" : "dés") +"activée."
                this.notificationsManager.add(NotifType.SUCCESS,  mess);
                this.upToDate = false;
            }, err => {
                console.error(err);
                this.notificationsManager.add(NotifType.ERROR, err);
            });
        }
        else {
            this.notificationsManager.add(NotifType.ERROR, "Alarme inexistante.");
        }
    }

    public synchroAlarmes(window: BrowserWindow) {
        if (!this.upToDate) {
            this.updateAlarmes().then(value => {
                window.webContents.send('alarmes', this.alarmes);
                this.synchroAlarmes(window);
            }, err => {
                console.error("Erreur lors de la mise à jour des alarmes");
            });
        } else {
            const currentDate = new CustomDate(new Date());
            const nowAlarmes = this.alarmes.filter(x => x.textDate === currentDate.timeInput && !x.disabled);
            if (nowAlarmes.length > 0) {
                window.webContents.send('now-alarmes', nowAlarmes);
            }
        }
    }

    private async updateAlarmes() {
        const alarmes = await this.dbManager.select("alarmes");
        if (!alarmes) {
            console.error("can't select table alarmes")
        }
        else {
            this.upToDate = true;
            this.alarmes = alarmes;
        }
    }


}

import { IpcMainEvent } from 'electron';
import { IpcChannel } from '../../../shared/IpcChannel';
import { IpcRequest } from '../../../shared/IpcRequest';
import { AlarmesManager } from '../../../main/AlarmesManager';
export class AddAlarmeChannel implements IpcChannel {

    getName(): string {
        return 'add-alarme';
    }

    handle(event: IpcMainEvent, request: IpcRequest): void {
        
        const alarmesManager: AlarmesManager = AlarmesManager.getInstance();
        const newAlarme = request.params[0];

        alarmesManager.addAlarme(newAlarme);

    }
}
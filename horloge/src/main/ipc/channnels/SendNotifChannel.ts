import { IpcMainEvent } from 'electron';
import { IpcChannel } from '../../../shared/IpcChannel';
import { IpcRequest } from '../../../shared/IpcRequest';
import { NotificationsManager } from '../../../main/NotificationsManager';

export class SendNotifChannel implements IpcChannel {

    getName(): string {
        return 'add-notif';
    }

    handle(event: IpcMainEvent, request: IpcRequest): void {

        const notificationsManager: NotificationsManager = NotificationsManager.getInstance();
        notificationsManager.add(parseInt(request.params[0]), request.params[1]);

    }
}
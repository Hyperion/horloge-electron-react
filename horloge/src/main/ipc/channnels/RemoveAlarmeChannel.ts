import { IpcMainEvent } from 'electron';
import { IpcChannel } from '../../../shared/IpcChannel';
import { IpcRequest } from '../../../shared/IpcRequest';
import { AlarmesManager } from '../../../main/AlarmesManager';

export class RemoveAlarmeChannel implements IpcChannel {

    getName(): string {
        return 'remove-alarme';
    }

    handle(event: IpcMainEvent, request: IpcRequest): void {
        
        const alarmesManager: AlarmesManager = AlarmesManager.getInstance();

        alarmesManager.removeAlarme(parseInt(request.params[0]));

    }
}
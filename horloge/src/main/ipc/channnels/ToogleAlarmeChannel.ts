import { IpcMainEvent } from 'electron';
import { IpcChannel } from '../../../shared/IpcChannel';
import { IpcRequest } from '../../../shared/IpcRequest';
import { AlarmesManager } from '../../../main/AlarmesManager';

export class ToogleAlarmeChannel implements IpcChannel {

    getName(): string {
        return 'toogle-alarme';
    }

    handle(event: IpcMainEvent, request: IpcRequest): void {
        
        const alarmesManager: AlarmesManager = AlarmesManager.getInstance();

        alarmesManager.toogleAlarme(parseInt(request.params[0]));

    }
}
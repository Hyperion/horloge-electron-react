import { IpcRenderer } from 'electron';
import { IpcRequest } from '../../shared/IpcRequest';

export class IpcService {
  private static instance: IpcService;

  constructor(){
  }

  public static getInstance(): IpcService {
    if (!IpcService.instance) {
      IpcService.instance = new IpcService();
    }

    return IpcService.instance;
}

  public send<T>(channel: string, request: IpcRequest): Promise<T> {

    if (!this.ipcRenderer) {
      this.initializeIpcRenderer();
    }

    if (!request.responseChannel) {
      request.responseChannel = `${channel}_response_${new Date().getTime()}`
    }
    const ipcRenderer = this.ipcRenderer;

    ipcRenderer.send(channel, request);

    // This method returns a  which will be resolved when the response has arrived.
    return new Promise(resolve => {
      ipcRenderer.once(request.responseChannel, (event, response) => resolve(response));
    });
  }

  public receive(channel: string, func: any) {

    if (!this.ipcRenderer) {
      this.initializeIpcRenderer();
    }

    const ipcRenderer = this.ipcRenderer;
    ipcRenderer.on(channel, (event, ...args) => func(...args));
  }

  private ipcRenderer?: IpcRenderer;

  private initializeIpcRenderer() {
    console.log("init ipc renderer");
    if (!window || !window.process || !window.require) {
      throw new Error(`Unable to require renderer process`);
    }
    this.ipcRenderer = window.require('electron').ipcRenderer;
  }
}
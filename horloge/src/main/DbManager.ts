import { Database } from 'sqlite3';
import { NotificationsManager } from './NotificationsManager';


export class DbManager {

    private static instance: DbManager;


    public static getInstance(): DbManager {
        if (!DbManager.instance) {
            DbManager.instance = new DbManager();
        }

        return DbManager.instance;
    }

    private db: Database;
    private notificationsManager: NotificationsManager;

    constructor() {
        this.notificationsManager = new NotificationsManager();
        this.db = new Database('.db/horloge.db', (err) => {
            if (err) {
                console.error("bdd error", err.message);
            }
        });
    }

    public select(table: string): Promise<any[]> {
        return new Promise<any[]>((resolve, reject) => {
            const sql = `SELECT * FROM ${table}`;
            this.db.all(sql, (err, rows) => {
                if (err) {
                    reject(err);
                }
                resolve(rows)
            });
        });
    }

    public add(table: string, objectToAdd: any): Promise<any> {
        // TODO faire en sorte que ça s'adapte à l'objet
        return new Promise<any>((resolve, reject) => {
            const sql = `INSERT INTO ${table}(textDate, disabled) VALUES ('${objectToAdd.textDate}', ${objectToAdd.disabled})`;
            this.db.run(sql, (err) => {
                if (err) {
                    reject(err.message);
                }
                resolve("item ajouté avec succès")
            });
        });
    }

    public update(table: string, id: number, fields: { column: string, value: string }[]): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            let setUpdate: string = "";

            for (const field of fields) {
                setUpdate += field.column + " = ";
                setUpdate += field.value + ", ";
            }
            setUpdate = setUpdate.slice(0, -2);

            const sql = `UPDATE ${table} SET ${setUpdate} WHERE id = ${id}`;
            this.db.run(sql, (err) => {
                if (err) {
                    reject(err.message);
                }
                resolve("item supprimé avec succès")
            });
        });
    }

    public remove(table: string, id: number): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            const sql = `DELETE FROM ${table} WHERE id = ${id}`;
            this.db.run(sql, (err) => {
                if (err) {
                    reject(err.message);
                }
                resolve("item supprimé avec succès")
            });
        });
    }

    public close(): boolean {
        this.db.close((err) => {
            if (err) {
                console.error(err.message);
                return false;
            }
            console.log("close database connection");
        });
        return true;
    }

}
